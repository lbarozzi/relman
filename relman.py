from flask import Flask,request,render_template,json,jsonify

app = Flask(__name__)

@app.route('/')
def main():
    mode=request.method
    # return {"string":"Hello, World!","mode":"On",}   # not working unless documented
    # return jsonify({"string":"Hello, World!","mode":"On",})
    return render_template('index.html')


@app.route('/user/<user>',methods=['GET','POST'])
def hellouser(user):
    return "Ciao %s in %s" % (user,request.method)


@app.route('/user/<user>/index.html')
def htmluser(user):
    return render_template('user.html',user=user)