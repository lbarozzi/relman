console.log("Starting Vue Admin app");
var app= new Vue( {
    el:'#uploadapp',
    data: {
        errorMsg: "",
        successMsg: "",
        showAddDoc: false,
        docType: "",
        token: "",
        types: [],
        files: [],
        downloads: [],
        showLogin: false,
        email: null,
        //secret: null,
        curlogin: 0,
        mylogin: null,
        status:0,
        stato_desc:"",
        uploadable:false,
        valid:{email:true,nome:false,cognome:false,residenza:false,telefono:false,cf:false },
        uri: "backend.php"
    },
    mounted() {
        var self=this;
        //this.token=$("#ticket").val();
        //console.log("Ticket: " + this.token);
        this.token= $("#tk").val();
        this.refresh();
        //Login
        this.getLogin();
    },
    computed: {
        /*
        showLogin: function() {
            if (mylogin!=null) {
                return mylogin.enabled=="1";
            } else {
                return true;
            }
        }
        //*/
    },
    methods: {
        getLogin(){
            var self=this;
            //console.log("Download token:"+ this.token);
            //axios({method: 'get', url: uri+'?tkn='+ self.token })
            $.post( this.uri, { ACTION: "getLogin", token: this.token } )
            .done(function( data ) {
                //self.downloads=JSON.parse(data);
                self.mylogin=JSON.parse(data);
                self.showLogin=(self.mylogin.enabled==0)? true:false; //!="1");
                self.status=self.mylogin.pratica_status;
                self.uploadable=(self.status==1)? true:false;
                if (self.status==1) { self.stato_desc="Aperta";}
                if (self.status==2) { self.stato_desc="In Lavorazione";}
                if (self.status==3) { self.stato_desc="Chiusa";}
                if (self.status==4) { self.stato_desc="Chiusa automaticamente";}
                console.log(self.mylogin);
            })
            .fail(function() {
                errorMsg= "error" ;
            });
            //
            setTimeout(this.validateLogin,1000);
        },
        makeurl(doc){
            return "backend.php?ACTION=getfile&idfile="+doc.idfile+"&token="+this.token;
            //return "backend.php?ACTION=getfile&idfile="+doc.idfile+"&token="+this.token+"&idlogin="+this.curlogin;
            //return "backend.php?ACTION=getfile&idfile="+doc.idfile+"&token="+this.token+"&idlogin="+this.curlogin.idlogin;
        },
        validateLogin(){
            if (this.mylogin==null) return false;
            //console.log("validateLogin");
            this.mylogin.cf=this.mylogin.cf.toUpperCase();
            var TextIn= new RegExp("[a-zA-Z0-9]+");
            var CF = new RegExp("[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]");
            var Tel= new RegExp("[0-9]+");
            this.valid.nome=TextIn.test(this.mylogin.nome);
            this.valid.cognome=TextIn.test(this.mylogin.cognome);
            this.valid.residenza=TextIn.test(this.mylogin.residenza);
            this.valid.cf=CF.test(this.mylogin.cf.toUpperCase());
            this.valid.telefono= Tel.test(this.mylogin.telefono);
            var result=true;
            for (var element in this.valid){
                //console.log("Test: "+ element);
                result= result && this.valid[element];
            }
            //console.log("Result: "+ result);
            return result;
        },
        closeDoc() {
             //console.log("Update pratica!");
             if (confirm('Chiudendo la pratica non sarà più possibile inserire altri documenti.\n Sei sicuro?')) {
                // Save it!
            } else {
                // Do nothing!
                return ;
            }
             this.mylogin.pratica_status=2;
             this.uploadable=false;
             $.post( this.uri, { ACTION: "updatePratica", token: this.token, json: JSON.stringify(this.mylogin) } )
             .done(function(data) {
                     //Check
                     //Refresh data
                     //self.getLogin();
             })
             .fail(function() {
                 errorMsg= "error" ;
             });
        },
        doneLogin(){
            var self=this;
            if (!this.validateLogin()){
                return false;
            }
            $.post( this.uri, { ACTION: "updateLogin", token: this.token, json: JSON.stringify(this.mylogin) } )
            .done(function(data) {
                    //Check
                    //Refresh data
                    self.getLogin();
            })
            .fail(function() {
                errorMsg= "error" ;
            });
        },
        login(){
            var self=this;
            var tkn='';
            //axios({method: 'get', url: uri+'?tkn='+ self.token })
            $.post( this.uri, { ACTION: "getToken", email: this.email, secret:this.secret } )
            .done(function(data) {
                app.token=data;
                tkn=data;
                console.log("token:"+ data);
                if (self.token!='') {
                    self.showlogin=false;
                    //setTimeout(self.refresh,1000);
                    errorMsg="";
                    successMsg="Login!"
                } else {
                    errorMsg="Login Failed";
                }
            })
            .fail(function() {
                errorMsg= "error" ;
            });
            this.token=tkn;
            console.log("End login token:"+ this.token);
            setTimeout(this.refresh,1000);
        },
        refresh(){
            console.log("Refresh token:"+ this.token);
            setTimeout(this.refresh,30000);
            if(this.token!=''){
                //setTimeout(this.refresh,3000);
                this.getFiles();
                this.getAllType();
                this.getDownloads();
            } else {
                this.types=[];
                this.files=[];
                this.downloads=[];
            }
        },
        delDoc(idupload){
            var self=this;
            //console.log("Donwload token:"+ this.token);
            //axios({method: 'get', url: uri+'?tkn='+ self.token })
            $.post( this.uri, { ACTION: "deletefile", token: this.token ,idfile:idupload} )
            .done(function( data ) {
                //self.downloads=JSON.parse(data);
                successMsg="File deleted";
            })
            .fail(function() {
                errorMsg= "error" ;
            });
            setTimeout(this.refresh,1000);
        },
        getDownloads(){
            var self=this;
            //console.log("Donwload token:"+ this.token);
            //axios({method: 'get', url: uri+'?tkn='+ self.token })
            $.post( this.uri, { ACTION: "downloadfile", token: this.token } )
            .done(function( data ) {
                self.downloads=JSON.parse(data);
            })
            .fail(function() {
                errorMsg= "error" ;
            });
            //if (this.downloads.length<1) { this.downloads=null;}
        },
        fileupload(){
            var self=this;
            var upfile = $('#uploadfile')[0].files[0];
            var filename = "" + $('#uploadfile').val();
            //try{
                let fd = new FormData();
                console.log("File size: " + upfile.size + " name: " + filename);
                console.log("fd"+ fd);
                fd.append("ACTION", 'uploadfile');
                fd.append("type", this.docType);
                fd.append('file', upfile);
                fd.append('token',this.token);
                //fd.append('idlogin',this.curlogin);
                fd.append('direction',0);
                console.log("fd"+ fd);
                $.ajax({
                    type: "POST",
                    url: this.uri,
                    processData: false,
                    contentType:false,
                    enctype: 'multipart/form-data',
                    data: fd,
                    success: function () {
                        successMsg="Data Uploaded: ";
                    }
                });
            //} catch(ops) { console.log("ops:"+ ops);}
        },
        addDoc(){
            this.showAddDoc=true;
            console.log("addDoc");
        },
        doneAddDoc(){
            var self=this;
            self.showAddDoc=false;
            this.fileupload();
            //TODO: fix
            setTimeout(this.refresh,1000);
            console.log("doneUpload");
        },
        getFiles(){
            var self=this;
            //console.log("Files token:"+ this.token);
            //axios({method: 'get', url: uri+'?tkn='+ self.token })
            $.post( this.uri, { ACTION: "getFileList", token: this.token } )
            .done(function( data ) {
                self.files=JSON.parse(data);
            })
            .fail(function() {
                errorMsg= "error" ;
            });
        },
        getAllType(){
            var self=this;
            //console.log("Types token:"+ this.token);
            var mytoken=this.token;
            $.post( this.uri, { ACTION: "getTypeList" , token: mytoken} )
            .done(function( data ) {
                self.types=JSON.parse(data);
            })
            .fail(function() {
                errorMsg= "error" ;
            });
        }
    },
    watch: { }
});
console.log(app.courses);